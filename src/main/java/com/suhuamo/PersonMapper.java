package com.suhuamo;

import com.suhuamo.mybatis.Param;
import com.suhuamo.mybatis.Select;

/**
 * @author yuanchuncheng
 * @slogan 不待春风慢，我以明月宴群山。
 * @date 2023-10-31
 * @description
 */
public interface PersonMapper {
    @Select("select * from person where id = #{id}")
    Person getPersonById(Integer id);
}
