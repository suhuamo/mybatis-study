package com.suhuamo;

/**
 * @author yuanchuncheng
 * @slogan 不待春风慢，我以明月宴群山。
 * @date 2023-10-31
 * @description
 */
public class Person {
    private Integer id;
    private String dog;
    private String color;

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", dog='" + dog + '\'' +
                ", color='" + color + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDog() {
        return dog;
    }

    public void setDog(String dog) {
        this.dog = dog;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
