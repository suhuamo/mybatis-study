package com.suhuamo.mybatis;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author yuanchuncheng
 * @slogan 今天的早餐是：早苗的面包、秋子的果酱和观铃的果汁~
 * @date 2023-10-31
 * @description 类型处理
 */
public interface TypeHandler<T> {

    /**
     * 向对应下标按照对应的数据类型插入内容
     * @param statement
     * @param idx
     * @param data
     */
    void setParameter(PreparedStatement statement, Integer idx, T data) throws SQLException;

    T getResult(ResultSet resultSet, String columnName) throws SQLException;


}
