package com.suhuamo.mybatis;

import com.sun.istack.internal.Interned;

import java.lang.annotation.*;

/**
 * @author yuanchuncheng
 * @slogan 今天的早餐是：早苗的面包、秋子的果酱和观铃的果汁~
 * @date 2023-10-31
 * @description
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface Select {

    String value();
}
