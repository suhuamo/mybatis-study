package com.suhuamo.mybatis;

import java.lang.reflect.Type;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author yuanchuncheng
 * @slogan 不待春风慢，我以明月宴群山。
 * @date 2023-10-31
 * @description
 */
public class StringHandler implements TypeHandler<String> {
    @Override
    public void setParameter(PreparedStatement statement, Integer idx, String data) throws SQLException {
        statement.setString(idx, data);
    }

    @Override
    public String getResult(ResultSet resultSet, String columnName) throws SQLException {
        return resultSet.getString(columnName);
    }
}
