package com.suhuamo.mybatis;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author yuanchuncheng
 * @slogan 不待春风慢，我以明月宴群山。
 * @date 2023-10-31
 * @description
 */
public class IntegerHandler implements TypeHandler<Integer>{
    @Override
    public void setParameter(PreparedStatement statement, Integer idx, Integer data) throws SQLException {
        statement.setInt(idx, data);
    }

    @Override
    public Integer getResult(ResultSet resultSet, String columnName) throws SQLException {
        return resultSet.getInt(columnName);
    }
}
