package com.suhuamo;

import com.suhuamo.mybatis.*;

import java.util.List;

/**
 * @author yuanchuncheng
 * @slogan 不待春风慢，我以明月宴群山。
 * @date 2023-10-31
 * @description 学生实体类查询数据接口
 */
public interface StudentMapper {

    // 创建mapper和查询sql语句注解、接口
    @Select("select * from student where age = #{age} and name = #{name}")
    List<Student> getStudentListByAge(@Param("name") String name, @Param("age") Integer age);

    @Select("select * from student where id = #{id}")
    Student getStudentById(@Param("id") Integer id);

    @Select("select count(*) from student")
    Integer getCount();

    @Select("select id from student")
    List<Long> getIds();

    @Select("select name from student where id = #{id}")
    String getNameById(@Param("id") Integer id);

    @Select("select name from student")
    List<String> getAllName();

    @Update("update student set name = #{name} where id = #{id}")
    Integer updateNameById(String name, Integer id);

    @Insert("insert into student value(#{id}, #{name}, #{age})")
    Integer insertStudent(Integer id, String name, Integer age);

    @Delete("delete from student where id = #{id}")
    Integer deleteStudentById(Integer id);
}
