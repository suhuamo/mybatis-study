package com.suhuamo;

import com.suhuamo.mybatis.MapperProxyFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yuanchuncheng
 * @slogan 不待春风慢，我以明月宴群山。
 * @date 2023-10-30
 */
public class Main {
    public static void main(String[] args) throws SQLException {
        StudentMapper studentMapper = MapperProxyFactory.getMapper(StudentMapper.class);
        Integer b1 = studentMapper.insertStudent(4, "桃花", 6);
        System.out.println("b1 = " + b1);
        Integer b = studentMapper.deleteStudentById(4);
        System.out.println("b = " + b);
        Integer 大大 = studentMapper.updateNameById("大大", 6);
        System.out.println("大大 = " + 大大);
        List<Long> ids = studentMapper.getIds();
        System.out.println("ids = " + ids);
        List<String> allName = studentMapper.getAllName();
        System.out.println("allName = " + allName);
        String nameById = studentMapper.getNameById(7);
        System.out.println("nameById = " + nameById);
        Integer count = studentMapper.getCount();
        System.out.println("count = " + count);
        Student studentById = studentMapper.getStudentById(3);
        System.out.println("studentById = " + studentById);
        List<Student> studentListByAge = studentMapper.getStudentListByAge("小花",23);
        System.out.println("studentListByAge = " + studentListByAge);

        PersonMapper personMapper = MapperProxyFactory.getMapper(PersonMapper.class);
        Person personById = personMapper.getPersonById(1);
        System.out.println("personById = " + personById);

    }
}